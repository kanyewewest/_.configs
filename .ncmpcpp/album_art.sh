#!/usr/bin/env bash

MUSIC_DIR=/media/disk/Music

COVER=/tmp/cover.jpg

function reset_background
{
    kitty icat --transfer-mode stream --clear --silent
}

function floating_math
{
	#floating_math $FUNCTION
	echo "scale=2;$1" | bc
}
function floor
{
	#floor $NUMBER
	echo "scale=0;$1/1" | bc
}

{
    album="$(mpc --format %album% current)"
    file="$(mpc --format %file% current)"
    album_dir="${file%/*}"
    [[ -z "$album_dir" ]] && exit 1
    album_dir="$MUSIC_DIR/$album_dir"
    
    covers="$(find "$album_dir" -type d -exec find {} -maxdepth 1 -type f -iregex ".*/.*\(${album}\|cover\|folder\|artwork\|front\).*[.]\(jpe?g\|png\|gif\|bmp\)" \; )"
    src="$(echo -n "$covers" | tail -n +1 | head -n1)"
    rm -f "$COVER"
    reset_background
    if [[ -n "$src" ]] ; then
        convert "$src" "$COVER"
	if [[ -f "$COVER" ]] ; then
	    #measure 1% in width/height from columns and lines	
	    width_1p="$(floating_math "$(tput cols)/100")"
	    height_1p="$(floating_math "$(tput lines)/100")"
	    width="$(floor "$width_1p*26")"
	    height="$(floor "$height_1p*50")"
	    pos_x="$(floor "$width_1p*2")"
	    pos_y="$(floor "$height_1p*30")"
	    #convert "$COVER" -resize "${width}x${height}" "$COVER"
	    kitty icat --silent --transfer-mode stream --scale-up --place "${width}x${height}@${pos_x}x${pos_y}" $COVER
        fi
    fi
} &
