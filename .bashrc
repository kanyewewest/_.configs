#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias sudo='sudo '
alias ls='ls --color=auto'
alias killcats='sh /home/brat/Documents/Projects/bash-testing/1.sh'
alias mountj='sh /home/brat/Documents/Projects/bash-testing/mountj.sh'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias snip='sh /home/brat/Documents/Pictures/Snips/snip.sh'

#PS1='[\u@\h \W]\$ '
PS1='\u\e[32m@[\e[0m\W\e[32m]\e[36m -> %\e[0m '
